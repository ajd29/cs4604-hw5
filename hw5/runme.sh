#!/bin/bash

cd CS4604-sql/hw.5-security.2
echo "Path is $(pwd)"

curl -o ./hearthstone.db.gz https://code.vt.edu/rquintin/CS4604-data/raw/master/hearthstone.db.gz
gunzip ./hearthstone.db.gz
cat hearthstone.db | docker run --rm -i --link postgres:postgres postgres psql -h postgres -U postgres

chmod u+x ./weapons
./weapons


echo "IMPORTANT: Before running the 'weapons' program you must change directory using the command listed below."
echo 
echo "  cd CS4604-sql/hw.5-security.2"
